<?php

namespace App\Http\Controllers;

use App\Communist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommunistController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $communist = Communist::create([
            'name' => $request->input('name')
        ]);
        return response()->json(['id' => $communist->id, 'name' => $communist->name], 200);
    }

    public function update(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'communistId' => 'required|exists:communists,id',
            'name' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $communist = Communist::find($request->input('communistId'));
        $communist->name = $request->input('name');
        $communist->save();
        return response()->json(['id' => $communist->id, 'name' => $communist->name], 200);
    }

    public function get(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'communistId' => 'required|exists:communists,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $communist = Communist::find($request->input('communistId'));
        return response()->json(['id' => $communist->id, 'name' => $communist->name], 200);
    }

    public function delete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'communistId' => 'required|exists:communists,id',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        Communist::destroy($request->input('communistId'));
        return response()->json(['message' => 'ok'], 200);
    }
}
