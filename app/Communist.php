<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communist extends Model
{
    protected $table = 'communists';
    protected $guarded = ['id'];
}
